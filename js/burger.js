function navResponsive() {
    let burger = document.querySelector(".burger");
    let nav = document.querySelector(".nav-links");
    burger.addEventListener("click", () => {
        nav.classList.toggle("nav-active");
        burger.classList.toggle("toggle");
    });
}
navResponsive();

