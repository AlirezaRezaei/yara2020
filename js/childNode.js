
var list_node = []
let id = 1;

function myNode(id, name, parent_id, parent, element) {
    this.id = id,
        this.name = name,
        this.children = [],
        this.addChild = addChild,
        this.remove_node = remove_node,
        this.parent_id = parent_id,
        this.element = element,
        this.parent = parent

    function addChild(child) {
        this.children.push(child)
    }

    function remove_node(id) {
        for (child in this.children) {
            if (this.children[child].id === id) {
                this.children.splice(child, 1);
            }
        }
    }

}

function newButton() {
    let element = document.createElement('div');
    let node = new myNode(0, `create new button`, -1, document.body, element);
    list_node.push(node)
    renderHtml(list_node)
}


function createChild(parent, parent_id) {
    let element = document.createElement('div');
    element.classList.add('parent')
    let node = new myNode(id, `element id = ${id} - parent id = ${parent_id}`, parent_id, parent, element);
    findParent(list_node, parent_id, node)
    renderHtml(list_node)
    id += 1;
}


function findParent(list_node, parent_id, node) {
    list_node.find(element => {
        if (element.id == parent_id) {
            element.addChild(node)
        } else {
            findParent(element.children, parent_id, node)
        }
    })
}


function renderHtml(nodes) {
    nodes.forEach(element => {
        renderDiv(element)
        if (element.children.length > 0) {
            renderHtml(element.children)
        }
    });
}

function renderDiv(el) {
    el.element.innerText = `${el.name}`;
    let add_child_button = document.createElement('button');
    add_child_button.innerHTML = "+"
    add_child_button.addEventListener("click", () => {
        createChild(el.element, el.id)
    })
    if (el.id != 0) {
        let remove_node = document.createElement('button');
        remove_node.innerHTML = "-"
        remove_node.addEventListener("click", () => {
            removeNode(list_node, el.id, el.parent_id)
        })
        el.element.appendChild(remove_node)
    }

    el.element.appendChild(add_child_button)
    el.parent.appendChild(el.element)
}

function removeNode(list_node, id, parent_id) {
    list_node.find(element => {
        if (element.id == parent_id) {
            element.remove_node(id)
            renderHtml(list_node)
        } else {
            removeNode(element.children, id, parent_id)
        }
    })
}


newButton();


