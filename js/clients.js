function createClient() {
    let clients = [
        {
            avatar: "01.jpg",
            quote:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.",
            name: "- Johnathan Doe",
        },
        {
            avatar: "02.jpg",
            quote:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.",
            name: "- Johnathan Doe",
        },
        {
            avatar: "03.jpg",
            quote:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.",
            name: "- Johnathan Doe",
        },
        {
            avatar: "04.jpg",
            quote:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.",
            name: "- Johnathan Doe",
        },
        {
            avatar: "05.jpg",
            quote:
                "'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.'",
            name: "- Johnathan Doe",
        },
        {
            avatar: "06.jpg",
            quote:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.",
            name: "- Johnathan Doe",
        },
    ];

    clients.forEach((element) => {
        let client_row = document.querySelector(".row_clients");
        let our_clients = document.createElement("div");
        our_clients.classList.add("col-1-of-3", "clients_card");
        let avatar = document.createElement("img");
        avatar.src = `./img/${element.avatar}`;
        avatar.classList.add("avatar");
        let info = document.createElement("div");
        info.classList.add("info");
        let quote = document.createElement("p");
        quote.classList.add('quote')
        let name = document.createElement("p");
        name.classList.add('client_name')
        quote.innerHTML = element.quote;
        name.innerHTML = element.name;
        info.appendChild(quote);
        info.appendChild(name);
        our_clients.appendChild(avatar);
        our_clients.appendChild(info);
        client_row.appendChild(our_clients);
    });
}

createClient();
